# Computational NAS: A System Build Guide

_2019.01.26_

This is a system build guide that suggests a hardware and software setup for dual use: A general purpose network attached storage (NAS) with formidable (Kappa) computational box.


## Contents
- [End Result](#end-result)
- [The Purpose of the Build](#the-purpose-of-the-build)
   * [Primary Needs](#primary-needs)
   * [Secondary Needs](#secondary-needs)
   * [Other Constraints](#other-constraints)
- [Hardware](#hardware)
   * [Chassis](#chassis)
   * [Motherboard](#motherboard)
   * [CPU](#cpu)
   * [Memory](#memory)
   * [Boot Drive](#boot-drive)
   * [Data Drives](#data-drives)
   * [PSU](#psu)
   * [Other](#other)
   * [Gaming vs Server-Grade](#gaming-vs-server-grade)
- [Software](#software)
   * [Automatic OS Installation](#automatic-os-installation)
   * [Boot Drive Partitioning](#boot-drive-partitioning)
   * [OS Installation](#os-installation)
   * [OS Install Script](#os-install-script)
   * [OS Initialization](#os-initialization)
   * [Testing the Data Drives](#testing-the-data-drives)
   * [The Data Pool](#the-data-pool)
   * [After Every Boot](#after-every-boot)
   * [Once a Month](#once-a-month)
   * [OS Update](#os-update)
- [Closing Remarks](#closing-remarks)
- [References](#references)


## End Result
Let's start by getting a glance at what we'll be getting ourselves into.  Below are a few illustrative photos of the build we will be discussing.

![A peak inside](img/01.jpg)
![The CPU cooler](img/04.jpg)
![The entrails](img/09.jpg)
![The outside](img/12.jpg)
![RAIDZ-2 info](img/raidz2.png)

All build photos can be found in the [img](img/) directory.


## The Purpose of the Build
### Primary Needs
This build addresses the following primary needs:

1. Secure data storage
2. Enabling computations

Data security will be based on three pillars: Parity, ZFS, and encryption.  Parity refers to storing data on multiple drives to prevent data loss in case of a drive (or drives) failure.  While parity can be achieved by simply mirroring drives, in this guide we go further and use five identical drives to build a system that survives a failure of up to two drives.  This is important, because when one of the drives dies, the remaining drives are under increased stress that may result in a failure of another drive.  In a system with single parity, that second drive failure leads to data loss.  Double parity gives more time for drive replacement and resilvering of the new drive largely alleviating the problem.

[ZFS](https://en.wikipedia.org/wiki/ZFS) is an advanced file system and logical volume manager.  There's plenty of good explanations of why ZFS is superior so I will let the reader do their own research on the subject.  Encryption is optional in general, but this guide assumes it is a desirable feature.

In terms of the second primary need, i.e., enabling computations, the nature of those computations isn't important.  Suffice it so say that the system should be able to walk and chew gum at the same time.  That said, training a large deep neural net is better done on specialized hardware configurations so keep your expectations in check.

### Secondary Needs
This build can address a series of secondary needs (e.g., web server, GitLab server, etc.), but we won't be elaborating on them here.

### Other Constraints
An additional constraint we will impose on our system (other than those articulated above in terms of primary needs) is that of physical footprint.  Namely, we want it to be relatively small.  The small size requirement determines the choice of the motherboard form factor: We will go with mini-ITX.

#### Size vs Airflow
As we know, with small chassis size comes great cooling responsibility.  At least two large-scale datacenter studies which looked at the correlation between temperature and failure rates have been published to date and the evidence seems inconclusive.  While the study run by Google found no indication of a significant correlation [3], the later study by Microsoft did find it [4].  Moreover, datacenter data collected by BackBlaze seem to be inconsistent with the correlation in question [1].  The crucial thing to note about these studies is that they all took place at datacenters and computer equipment gets a much better treatment at a datacenter then it does in an average household, especially when temperature is concerned.  For example, some of the temperature scales reported by the articles do not even go above 40C, while the temperatures of the data drives in our build is in the 40-50C range when idle.  To restrict that temperature, our build includes a 200mm intake case fan which according to my tests helps to limit the temperature to about 40C when idle (and not much more when under stress).

That said, the [datasheet](https://www.seagate.com/files/www-content/datasheets/pdfs/exos-7-e8-data-sheet-DS1957-1-1709US-en_US.pdf) for the drives used in this guide lists the operating temperature as 5-60C.  With or without the extra 200mm case fan, we will remain within these margins; the question is, how close to the upper bound of the range we want to be.


## Hardware
### Components List

- Chassis: **BitFenix Phenom M Nvidia Edition: Black MicroATX Mini Tower Case**
- CPU: **Intel Core i7-8700K 3.7 GHz 6-Core Processor**
- CPU cooler: **darkFlash Shadow PWM Aluminium CPU LED**
- Memory: Crucial **16GB DDR4 2400 (PC4-19,200) DIMM 288-Pin** (x2)
- Motherboard: **ASRock H370M-ITX/ac Mini ITX LGA1151**
- Boot drive: **Samsung 970 Evo 250 GB M.2-2280 Solid State Drive**
- Data drives: **Seagate Exos 7E8 8TB 512e SATA 512MB Cache 3.5-Inch Enterprice (ST8000NM0055)** (x5)
- PSU: **EVGA BR 450 W 80+ Bronze Certified ATX**
- Case fan: **BitFenix BFF-SPRO-20025KK-RP Cooling 200mm Case Fan**

### Chassis
The chassis needs to take mini-ITX motherboard and at least five 3.5'' drives.  The Phenom fits the bill perfectly.  It even has room for another 3.5'' drive and two 2.5'' drives, but we won't be needing that.  The chassis comes with the front intake and the back exhaust fans installed, both 140mm.

### Motherboard
There aren't very many mini-ITX motherboard out there so our choices are limited.  The ones that are available are either server-grade or gaming.  Either way is fine, but we will go consumer-grade.

### CPU
This comes down to personal needs.  There's plenty of reasonable choices and it's often the budget that will dictate it.  We will go with a current 6-core 12-threads Intel Core CPU.  We choose Core instead of server-grade Xeon to save some cash.  An added benefit of Core CPUs is the integrated video card.  The choice of the unlocked CPU (identified by the letter K) leaves some headroom for overclocking; might be useful but is non-essential.

One of the reasons to avoid AMD are reports of intermittent instability on FreeBSD.  This will surely get sorted out with time, but for the time being we will stay away from the otherwise very welcome offer of the Intel's hegemony breaker.

In terms of CPU cooler, even though the system may be out of sight, we'll make it stylish on the inside by adding an LED fan.  The fan is visible through the mesh top of the chassis.  Any good cooler will do though.

### Memory
Anything starting at 16GB is going to be reasonable, but we will max the system out with 32GB because it will benefit the ARC (adaptive replacement cache; the ZFS in-memory read cache), enable more memory-intensive computations, and virtual machines.

### Boot Drive
We need an SSD here because this thing needs to be fast to mediate ZFS reads and writes.  Using a dedicated drive or even memory for ZIL (ZFS intent log) results in better performance, but we will keep it basic (and affordable).  Fortunately, our motherboard supports booting from NVDe (via UEFI) and FreeBSD supports NVDe as well, so that's what we're going to go with.  An important caveat is that the motherboard will disable one of its SATA 3 channels if NVMe drive is present, so five data drives is all we will get on this particular system.

### Data Drives
Feel free to select drives with capacity that matches your anticipated needs; this guide doesn't make any assumptions regarding the size.  Keep in mind though that it is recommended to keep ZFS pool space under 80-90% utilization to maintain pool performance.  We will assume five 8TB drives.  With the statistics published by BackBlaze [2] in mind, we will choose the drives that have been well-tested (i.e., have large sample size) and have low annualized failure rate (AFR).  Seagate Exos 7E8 (ST8000​NM0055) looks like a solid pick.  Being an enterprise drive, it comes with five-year warranty, something that's always good to have.

### PSU
Without a powerful GPU, this system doesn't need a lot of power.  It might take about 120W under full load, but there should always be some headroom.  We will go with a reputable brand and get whatever's currently on sale and has at least 200-300W.

### Case fan
This is an optional component, but if we use the two 140mm case fans the chassis comes with, the temperature of the two bottom drives will hover around 50C.  By replacing the intake fan with the 200mm one, we will get about 40C on all five drives.  Note that fans 25mm thick or thinner will fit in this case without issues; thicker fans may collide (I haven't actually tested that) with the drives cage.  Also note that the 200mm fan is a bit louder than a 140mm one.  However, because this is a server and as such it might stand somewhere out of sight, it shouldn't be a problem.

### Other
We're almost done, but we will need a few more thing.  First, five SATA 3 cables; the motherboard comes with two.  Second, a network cable; you probably don't want your NAS to connect to the internet wirelessly.  Third, a keyboard and a screen to do the installation; after that, the system will run headless.  Finally, we will need a USB flash drive to etch the OS installer onto.  Note that our motherboard only has HDMI output (two of them, in fact) so you may need that cable too if you don't have one already.

### Gaming vs Server-Grade
A quick note on choosing between gaming/consumer and server-grade motherboard, CPU, and memory.  In this guide, we don't go server-grade route for two reasons: (1) To save some money and (2) because this is a home server.  If you're building a system you don't have easy access too, server-grade components are a wiser choice.  The last thing you want is for your motherboard or CPU to die because you've been riding them too hard and now you need to book a flight to Antananarivo to fix what shouldn't have broken in the first place.


## Software
In terms of software, we will go with [FreeBSD](https://www.freebsd.org/) 12.0R, which has been released just last month (i.e., Dec 2018).  Fully baked FreeBSD-based solutions with a pretty Web interface and plenty of useful apps (e.g., [FreeNAS](https://freenas.org/)) are available, but for simplicity we will go as bare-bones as possible.  You can get FreeBSD [here](https://download.freebsd.org/ftp/releases/amd64/amd64/ISO-IMAGES/12.0/) (`memstick` is what you want; `xy` denotes compressed images).

### Automatic OS Installation
FreeBSD has an excellent installer that will guide you through the process painlessly.  Feel free to go that route if you'd prefer.  A fully automatic install, however, won't partition the boot drive with ZFS log and cache in mind.  Perhaps the best way to blend the installer with our particular needs is to jack-out to shell to partition the boot drive manually and rely on the installer otherwise.  If that seems like something that appeals to you, you will need the commands from the next subsection (i.e., Boot Drive Partitioning).  Moreover, you will also need to add `geom_eli_load="YES"` to `/boot/loader.conf`.

Of course, there also is the option of doing a full automatic installation.  The most important ramification of that will be the lack of ZIL and L2ARC devices which will result in degraded performance (mostly reads) of your data pool.

### Boot Drive Partitioning
First, we'll partition the boot drive and install the boot code:
```
gpart destroy -F nvd0
gpart create -s gpt nvd0

# UEFI boot:
gpart add -t efi -l efiboot -a 4k -s 800K nvd0
gpart bootcode -p /boot/boot1.efifat -i 1 nvd0

# Install pMBR anyway because it's small and allows boot on BIOS machines:
gpart add -t freebsd-boot -l gptboot -a 4k -s 512k nvd0
gpart bootcode -b /boot/pmbr -p /boot/gptzfsboot -i 2 nvd0

# Add the rest of partitions:
gpart add -t freebsd-swap -l swap  -a 4k -b 1M -s  32G nvd0
gpart add -t freebsd-zfs  -l root  -a 4k       -s  32G nvd0
gpart add -t freebsd-ufs  -l log   -a 4k       -s   4G nvd0
gpart add -t freebsd-ufs  -l cache -a 4k       -s 120G nvd0
```
As you can see, we dedicate 32G to the base system partition (`root`) and will be going with ZFS-on-root.  32G may seem like an overkill, especially that a ZFS pool can be grown later if needed (it cannot be shrunk though!), but it's a bit of a cumbersome process so we will just allocate more space right away; we have the space for it anyway.

The `log` and `cache` partitions are to make ZFS run smoother.  Using one SSD for both is always going to be contentious topic (and for good reasons), but that's all we have.  With respect to cache size, 120GB is the max of what I'd recommend for 32GB of RAM.  If you elect to build a system with 16GB or RAM, I wouldn't even bother with L2ARC (unless in a system dedicated to storage only).

Note also that we leave 45GB free at the end of the SDD for over-provisioning.  It appears that opinions are still split on this.  I don't have time to get to the bottom of this at the moment and choose to stay on the safe side in terms of the SSDs longevity.  You may want to do your own research.

### OS Installation
Next, we create the ZFS root file system (`zroot`) and install the OS:
```
dir_dst=/var/zroot

mount -rw /

# Create zroot:
dir_dst=/var/zroot

gnop create -S 4096 /dev/gpt/root
zpool create -o altroot=$dir_dst -o cachefile=/var/tmp/zpool.cache zroot /dev/gpt/root.nop
zpool export zroot
gnop destroy /dev/gpt/root.nop
zpool import -o altroot=$dir_dst -o cachefile=/var/tmp/zpool.cache zroot

zpool set bootfs=zroot zroot

zfs set checksum=fletcher4 zroot
zfs set compression=lz4 zroot
zfs set atime=off zroot
zfs set mountpoint=/ zroot

# Install the OS:
cd $dir_dst
ln -s usr/home home
tar xvJpf /usr/freebsd-dist/base.txz
tar xvJpf /usr/freebsd-dist/kernel.txz
tar xvJpf /usr/freebsd-dist/lib32.txz

cp /var/tmp/zpool.cache $dir_dst/boot/zfs/zpool.cache
```

Next, we'll do some basic configuration (fill in your values below):
```
hostname=...
nic_name=...    # e.g., "em0"
nic_config=...  # e.g., "DHCP" or "inet 192.168.0.1 netmask 255.255.255.0"

echo 'sendmail_enable="NO"'           >> $dir_dst/etc/rc.conf
echo 'sendmail_submint_enable="NO"'   >> $dir_dst/etc/rc.conf
echo 'sendmail_outbound_enable="NO"'  >> $dir_dst/etc/rc.conf
echo 'sendmail_msp_queue_enable="NO"' >> $dir_dst/etc/rc.conf
echo 'hostname="'$hostname'"'         >> $dir_dst/etc/rc.conf
echo 'ifconfig_'$nic'="'nic_config'"' >> $dir_dst/etc/rc.conf
echo 'zfs_enable="YES"'               >> $dir_dst/etc/rc.conf
echo 'sshd_enable="YES"'              >> $dir_dst/etc/rc.conf
echo 'syslogd_enable="YES"'           >> $dir_dst/etc/rc.conf
echo 'syslogd_flags="-ss"'            >> $dir_dst/etc/rc.conf
echo 'clear_tmp_enable="YES"'         >> $dir_dst/etc/rc.conf
echo 'powerd_enable="YES"'            >> $dir_dst/etc/rc.conf
echo 'powerd_flags="-a hadp"'         >> $dir_dst/etc/rc.conf

echo 'zfs_load="YES"'      >> $dir_dst/boot/loader.conf
echo 'geom_eli_load="YES"' >> $dir_dst/boot/loader.conf

echo -e '# Device\tMountpoint\tFStype\tOptions\tDump\tPass#' >> $dir_dst/etc/fstab
echo -e '/dev/gpt/swap\tnone\tswap\tsw\t0\t0'                >> $dir_dst/etc/fstab

chroot $dir_dst
```

Let's set the times zone:
```
cp $dir_dst/usr/share/zoneinfo/America/New_York $dir_dst//etc/localtime
```

Finally, let's change the root password and add a regular user:
```
username=...  # your user's login

passwd

pw useradd -n $username -s /bin/csh -m -G wheel
passwd $username
```

This is where you reboot into your freshly installed OS (don't forget to remove that USB drive):
```
reboot
```

### OS Install Script
As an aside, to save some time in the future, you could put the commands from the two previous subsections in a script, customize it to your needs, publish somewhere, and download onto a USB drive FreeBSD installer by selecting `Shell` when it asks and running:
```
mount -rw /
/sbin/dhclient em0
fetch --no-verify-peer <script-url>
sh <script-name>
```

### OS Initialization
At this point you should be able to log into your OS.  You can also SSH into it as the SSH daemon will be running.  It's time to mold the system to our needs even more:
```
portsnap fetch
portsnap extract
portsnap update

pkg
pkg update
pkg install -y sudo bash tmux rsync nano emacs-nox htop iftop lsof smartmontools syncthing zfs-stats
```
Remove the packages you won't use and add ones you will.  Also, downloading the ports tree is optional, but I like to have it in case I need it.

As the last step, I'd suggest adding your user to the list of sudoers by running `sudo visudo` and adding the following line to the file:
```
<your-username> ALL=(ALL) ALL
```

If you get stuck in vi, [this blog post](https://stackoverflow.blog/2017/05/23/stack-overflow-helping-one-million-developers-exit-vim) is not only going to help you but will also brighten your day in more than one way.

### Testing the Data Drives
This is where you roll out your hard-drive testing routines.  In this guide, we will limit ourselves to three SMART tests per drive.  You want to run them in sequence.  The _conveyance_ test, which takes about two minutes, is designed to determine if the drive has been damaged during transport and we will run it first.  The _short_ test, which takes another two minutes or so, will do a cursory test of the drive's operational features; we run it second.  Finally, the _long_ test, which for an 8TB drive takes nearly 12 hours, will do a complete test of the drive's geometry.  All those tests are off-line, meaning that you can use the drives while the tests are under way.  That is relevant for an operational system that we would want to do a health-check on once in a while.

The tests are run like so:
```
for i in 0 1 2 3 4; do smartctl -t conveyance /dev/ada${i}; done
for i in 0 1 2 3 4; do smartctl -t short      /dev/ada${i}; done
for i in 0 1 2 3 4; do smartctl -t long       /dev/ada${i}; done
```
Again, I ran these tests in sequence (i.e., conveyance first, short second, and long third); I haven't tried scheduling them all at once but it might work as well.  To check the test progress and results, issue `smartctl -a /dev/ada0` for each of the drives and inspect the `SMART Self-test log structure` section of the output.

I'd recommend waiting for the long tests to complete before continuing.

### The Data Pool
Finally, it's time to create the ZFS pool that will store all that data.  The following commands create a [RAID-Z2](https://en.wikipedia.org/wiki/Non-standard_RAID_levels#RAID-Z) (i.e., ZFS RAID with double parity) pool over our five data drives.  Because we are electing to encrypt the content, [Geli](https://www.freebsd.org/cgi/man.cgi?geli(8)) (our invincible encryption genie) will ask for password three times per disk so get those fingers warmed up.  As a side note, native ZFS encryption on FreeBSD is [in the works](https://lists.freebsd.org/pipermail/freebsd-current/2018-August/070832.html).
```
kldload geom_eli

# The pool:
for i in 0 1 2 3 4; do
    gpart destroy -F /dev/ada${i}
    gpart create -s gpt ada${i}
    gpart add -a 4k -b 1M -t freebsd-ufs -l data${i} ada${i}

    geli init -s 4096 -e AES-XTS -l 256 /dev/gpt/data${i}
    geli attach /dev/gpt/data${i}

    let i=${i}+1
done

zpool create zdata raidz2 /dev/gpt/data0.eli /dev/gpt/data1.eli /dev/gpt/data2.eli /dev/gpt/data3.eli /dev/gpt/data4.eli

# Log and cache:
for dev in log cache; do
    geli init -s 4096 -e AES-XTS -l 256 /dev/gpt/$dev
    geli attach /dev/gpt/$dev
done

zpool add zdata log   gpt/log.eli
zpool add zdata cache gpt/cache.eli
```
Note that if you chose to do the fully automatic OS installation earlier, you won't have the `log` and `cache` devices so you will need to skip that part of the script.

Let's make sure the pool is up by issuing `zpool status zdata` command which should produce the following output:
```
  pool: zdata
 state: ONLINE
  scan: none requested
config:

        NAME               STATE     READ WRITE CKSUM
        zdata              ONLINE       0     0     0
          raidz2-0         ONLINE       0     0     0
            gpt/data0.eli  ONLINE       0     0     0
            gpt/data1.eli  ONLINE       0     0     0
            gpt/data2.eli  ONLINE       0     0     0
            gpt/data3.eli  ONLINE       0     0     0
            gpt/data4.eli  ONLINE       0     0     0
        logs
          gpt/log.eli      ONLINE       0     0     0
        cache
          gpt/cache.eli    ONLINE       0     0     0

errors: No known data errors
```

Running `zpool list` should output something like this:
```
NAME    SIZE  ALLOC   FREE  CKPOINT  EXPANDSZ   FRAG    CAP  DEDUP  HEALTH  ALTROOT
zdata  36.2T  15.1M  36.2T        -         -     0%     0%  1.00x  ONLINE  -
zroot  31.5G  1.76G  29.7G        -         -     0%     5%  1.00x  ONLINE  -
```

Next, we will create ZFS datasets that will live inside of our pool.  It is useful to think of a dataset as a separate filesystem in that access rights can be defined on a dataset level in a more fine-grain fashion than they can on a directory level.  Moreover, ZFS datasets (or more correctly, their snapshots) can be streamed to another ZFS pool (local or remote) via `zfs send` and `zfs receive`.  You can do other cool things with datasets too, for example, specify a quota.  It is also important to note that moving data between datasets will require actual physical rewrite.  This is in contrast to moving data within a dataset which is very fast.  Consequently, it would behoove you to plan your dataset decomposition sensibly (although you can always change it later).

The following will create some example dataset for us:
```
zfs create zdata/data
zfs create zdata/media
zfs create zdata/media/music
zfs create zdata/media/photos

zfs set compression=lz4 zdata

zfs set mountpoint=/mnt/data  zdata/data
zfs set mountpoint=/mnt/media zdata/media

zfs set quota=500G zdata/data  # only an example
```

After you're done creating the datasets, don't forget to change the owner:
```
chown -R <your-username>:<your-username> /mnt/*
```

### After Every Boot
Every time the systems comes up, we will need to attach all the Geli devices, decrypt them to get access to the respective ZFS partitions, and import the pool.  That's seven times typing the password, but I prefer that to the alternative.  You could use a keyfile, but I personally don't want to bother with that.
```
geli attach /dev/gpt/log
geli attach /dev/gpt/cache

geli attach /dev/gpt/data0
geli attach /dev/gpt/data1
geli attach /dev/gpt/data2
geli attach /dev/gpt/data3
geli attach /dev/gpt/data4

zpool import zdata
```

### Once a Month
To identify data integrity problems, we will scrub the pool periodically.  Once a month is going to be sufficient for our drives, but once a week would be wiser for consumer drives.
```
zpool scrub zdata
```

### OS Update
We will keep our system up to date with a periodic sprinkle of the following spices:
```
freebsd-update fetch
freebsd-update install

pkg audit -F
pkg upgrade
pkg autoremove
pkg update
pkg clean -y
```

## Closing Remarks
I wanted to close by emphasizing that ZFS is not a one-size-fits-all solution, but the setup presented above is a good starting point.  You should consider your unique workload and test your setup to make sure you get all you can out of your system and are not harming performance.  If you prefer FreeNAS over FreeBSD, I'd look for ZFS recommendations on FreeNAS-dedicated fora; this guide doesn't assume streaming to be the major purpose of the system.

That's all, Folks!  Hope this helps and good luck setting up some sweet NAS!


## References
1. BackBlaze (2014) Hard Drive Temperature – Does It Matter? [Web site](https://www.backblaze.com/blog/hard-drive-temperature-does-it-matter)
2. BackBlaze (2018) Hard Drive Stats for Q3 2018: Less is More. [Web site](https://www.backblaze.com/blog/2018-hard-drive-failure-rates)
3. Pinheiro E, Weber W-D, & Barros LA (2007) Failure Trends in a Large Disk Drive Population. _FAST_. [PDF](https://static.googleusercontent.com/media/research.google.com/en//archive/disk_failures.pdf)
4. Sankar S, Shaw M, Vaid K, & Gurumurthi S (2013) Datacenter Scale Evaluation of the Impact of Temperature on Hard Disk Drive Failures. _ACM Transactions on Storage (TOS), 9(2)_. [PDF](https://www.cs.virginia.edu/~gurumurthi/papers/acmtos13.pdf)
