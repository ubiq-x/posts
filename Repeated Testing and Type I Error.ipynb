{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Repeated Testing and Type I Error\n",
    "\n",
    "This notebook simulates repeated sampling-and-testing procedure to elucidate how that influences the type I error.\n",
    "\n",
    "Let's start by definig the parameters of the sampling distribution:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy import stats\n",
    "\n",
    "pop_m = 0   # population mean\n",
    "pop_sd = 1  # population standard deviation\n",
    "n = 100     # sample size"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's draw one sample of size `n` from the normal distribution parameterized as above and compare the mean of that sample to the population mean using the t-test:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Ttest_1sampResult(statistic=1.1820305821143966, pvalue=0.24002494329436472)"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "sample = stats.norm.rvs(loc=pop_m, scale=pop_sd, size=n)\n",
    "stats.ttest_1samp(sample, popmean=pop_m)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The test statistic and the *p*-value associatied with the test are shown above.  Let's execute the same sample-and-test procedure multiple times and check how many of the resulting *p*-values are below our \\\\(\\alpha\\\\)-level (we will call that proportion *false discovery rate* or *FDR*):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.0488"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "m = 10000      # number of times to perform repeat the sample-and-test procedure\n",
    "p_values = []  # list of p-values\n",
    "alpha = 0.05   # alpha level\n",
    "\n",
    "for _ in range(m):\n",
    "    sample = stats.norm.rvs(loc=pop_m, scale=pop_sd, size=n)\n",
    "    p_values.append(stats.ttest_1samp(sample, popmean=0).pvalue)\n",
    "\n",
    "fdr = sum(p < alpha for p in p_values) / m  # false discovery rate\n",
    "fdr"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we can see, even though all the samples were taken from the same population, the mean of about 5% of them is judged to be significantly different than the population mean which clearly isn't the case.\n",
    "\n",
    "Of course, in the paper we are writing we don't do tens of thousands comparisons.  Also, the comparisons we make make sense beause they are guided by theoretical expectations, i.e., we are not fishing for hypotheses.  Moreover, we do not adhere to any particular \\\\(\\alpha\\\\)-level; we simply use the *p*-value as a gauge of the strenght of evidence much like it has been suggested by Ronald Fisher who created the *p*-value.  In that respect, lower *p*-values can be interpreted as stronger evidence against the null-hypothesis and we never need to resort to the mechanistic operationalization of the *p*-value as proposed by the null-hypothesis significance testing framework (NHST).  In NHST, changing the *p*-value may result in changing the __decision__; there are no decisions in the Fisher's conceptualization of the *p*-values.\n",
    "\n",
    "At the end of the day, all that is just my interpretation though :-)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "To make experimenting with different values easier, let's define a function that will crunch the FDR in one swift swoop (the values in the first line after the equal sign are the default values):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "def comp_fdr(pop_m=0, pop_sd=1, n=100, m=10000, alpha=0.05):\n",
    "    p_values = []\n",
    "    for _ in range(m):\n",
    "        sample = stats.norm.rvs(loc=pop_m, scale=pop_sd, size=n)\n",
    "        p_values.append(stats.ttest_1samp(sample, popmean=0).pvalue)\n",
    "    return sum(p < alpha for p in p_values) / m"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's call the function with all arguments set to their default values:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.0496"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "comp_fdr()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And now let's change the sample size and the \\\\(\\alpha\\\\)-level:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.0105"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "comp_fdr(n=1000, alpha=0.01)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The ratio of incorrectly rejected null hypotheses is independent from the sample size; it only depends on the alpha level."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
